%! TEX program = lualatex

\documentclass[a4paper, 12pt, oneside]{memoir}
\OnehalfSpacing
\pagestyle{simple}
\usepackage{microtype}

\setlength\parindent{0pt}
\setlength\parskip{\bigskipamount}

\usepackage{fontspec}
% \setmainfont[Renderer=ICU,BoldFont={Gentium Basic Bold},BoldItalicFont={Gentium Basic Bold Italic}]{Gentium Plus}
\setmainfont[BoldFont={Gentium Basic Bold},BoldItalicFont={Gentium Basic Bold Italic}]{Gentium Plus}
\setmonofont[Scale=MatchLowercase]{Iosevka}
\newcommand{\C}[1]{\emph{#1}}
\newcommand{\E}[1]{\emph{#1}}
\newcommand{\foreign}[1]{\emph{#1}}
\newcommand{\work}[1]{\emph{#1}}
\newcommand{\gram}[1]{\textsc{#1}}
\newcommand{\optional}[1]{{\small [#1]}}

\newcommand{\click}{{\fontspec{Symbola}⏩}}
%\newcommand{\click}{{\fontspec{Symbola}▶}}
\newcommand{\anticlick}{{\fontspec{Symbola}⏪}}

\usepackage{xcolor}
\definecolor{todocolor}{HTML}{EE1111}
\newcommand{\todo}[1]{\textcolor{todocolor}{#1}}

\usepackage[super]{nth}

\usepackage{enumitem}
\setlist[itemize]{noitemsep,nolistsep}

\usepackage{needspace}
\newcommand{\partitle}[1]{{\needspace{2\baselineskip}\large\textsc{\lowercase{#1}}}}
%\newcommand{\partitle}[1]{\uppercase{#1}}

\begin{document}
\OnehalfSpacing

Hi,

\partitle{Meta}
\begin{itemize}
	\item Before we start, \click{} the slides are available at this address \emph{(also here)}.

	\item This talk is a broad overview of one chapter from my PhD thesis, which you can find on my website: \click{} just remove the part after the question mark.
\end{itemize}


\partitle{Etymology}
\begin{itemize}
	\item \click{} \click{} Let’s begin. \click{} What is \emph{text}?
		This is a big question with many answers, but I think etymology can give us some insight.

	\item The English word \E{text} ultimately comes from Latin \click{} \foreign{textus}, meaning literally ‘that which is woven’ and by extension ‘the style or tissue of a literary work’.
		Related Latin words include \click{} \foreign{textilis} ‘woven, intertwined’ (\E{textile}), and \click{} \foreign{textūra} ‘weaving’ (\E{texture}).
\end{itemize}


\partitle{Narrative modes}
\begin{itemize}
	\item \click{} So texts are metaphorical textiles which are made of multiple threads.
		What exactly are these threads and how they are interwoven (/ˈɪntəɹwəʊvən/) depends on the type of the text.
		Here we focus on short stories.

	\item \click{} According to Bonheim’s theory of narrative modes, the following four modes are the principle elements of narrative texts:

		\begin{itemize}
			\item \click{} \emph{description}, which provides details regarding the narrated scene and its participants;

			\item \click{} \emph{report}, which unfolds the plot and makes the staple mode of narration;

			\item \click{} \emph{speech}, which represents dialogue between characters;

			\item and \click{} \emph{comment}, which is where the author discusses the narrative and comments upon it, somewhat externally.
		\end{itemize}
\end{itemize}


\partitle{Topic}
\begin{itemize}
	\item \click{} Of the four narrative modes, this talk deals with \click{} the \emph{seams} between two of them: the diegetic \emph{report} and the mimetic \emph{speech}.

	\item More specifically, the main focus is the fundamental grammatical features of the linguistic expression of the meeting point between the two; or in other words, the linguistic expression that signals, embeds and introduces direct reported speech into the broader narrative discourse.
		\click{} This is referred to here by the term \emph{quotative index}, after Güldemann, \click{} but a plethora (/ˈplɛθəɹə/) of other terms occur in the literature as well.

	\item \click{} Here is a simple example which has all the basic components present.
		The main division is between the \click{} \emph{quote} (the direct speech; \emph{Q} for short) and the \click{} \emph{quotative index} (\emph{QI} for short), which can be subdivided into:

		\begin{itemize}
			\item \click{} a \emph{nucleus} (/ˈnjuː.kli.əs/);
			\item \click{} a \emph{speaker};
			\item \click{} an addressee;
			\item and \click{} a \emph{modification} component, which covers expressions that nuance the manner, time or purpose of speech.
		\end{itemize}

		% \click{} These five colours recurs throughout the presentation, so keep them in mind.
\end{itemize}


\partitle{\click{} Corpus}
\begin{itemize}
	\item \click{} The research presented here studies modern Literary Welsh, and \click{} the corpus consists of two collections of short stories by Kate Roberts: \click{} \C{Te yn y Grug} and \C{Haul a Drycin}.

	\item The stories portray the childhood and adolescence of two girls growing up in Caernarfonshire, the area where the author spent her formative years.

	\item The system that emerges from the corpus is consistent, coherent and subtle, exhibiting Roberts’ masterful use of language.
\end{itemize}


\partitle{Structure}
\begin{itemize}
	\item \click{} Having laid the foundations, we proceed to the main part: describing the linguistic features of reporting speech.

	\item Three topics are dealt with:

		\begin{itemize}
			\item \click{} the distinct \emph{syntactic patterns} (or \emph{types}) of quotative indexes and their text-linguistic signifieds;

			\item \click{} the \emph{nucleus} of the quotative indexes;

			\item \click{} and the distribution of \emph{overt and zeroed} quotative indexes of the first type.
				(This will be explained in due course; don’t worry…)
		\end{itemize}
\end{itemize}


\partitle{\click{} QI1, QI2, and QI3}
\begin{itemize}
	\item \click{} Quotative indexes occur in three syntactic patterns or types, referred to here as QI1, 2 and 3.

	\item \click{} These are distinct in form and in function.
		\begin{itemize}
			\item By \emph{form} I mean both their syntagmatic structure as well as the paradigmatic properties of the slots in the patterns.

			\item By \emph{function} I mean the signified of the choice between the three: what does the author signal by choosing one type over the others.
		\end{itemize}
\end{itemize}


\partitle{Syntagmatics}
\begin{itemize}
	\item \click{} How do they differ in form?

	\item \click{} There are too many details here to go over, but the main point is that:
		\begin{itemize}
			\item \click{} QI1 \click{} occupies its own paragraph and \click{} has the quote as the first element in that paragraph.
				\click{} In QI1 the quote can also appear on its own, with a \emph{zeroed} quotative index.

			\item \click{} QI2 \click{} has the quotative index as the coda of a previous narrative paragraph, \click{} preposed \emph{before} the quote, from which it is separated by a paragraph break.

			\item And \click{} QI3 \click{} is embedded within the flow of a narrative paragraph.
		\end{itemize}
\end{itemize}


\partitle{Examples}
\begin{itemize}
	\item \click{} Let’s look at three examples from the corpus so we have a better grasp.

	\item \click{} Here is a fragment of a dialogue, with two consecutive instances QI1:

		\begin{itemize}
			\item \click{} It has a line of dialogue \click{} with an overt intraposed QI1,
			\item \click{} followed by a line with a zeroed QI1.
		\end{itemize}

	\item \click{} Here is an example for QI2: \click{} it has a \emph{narrative paragraph}, \click{} with a \emph{quotative index} as its coda, \click{} followed by and an uninterrupted quote in a separate paragraph.

	\item \click{} Here we can see a \click{} narrative paragraph, into the flow of which speech is embedded as QI3, with \click{} a nucleus  and \click{} a quote.
\end{itemize}



\partitle{Function}
\begin{itemize}
	\item \click{} So these are the formal syntagmatic differences between the three types. \item \click{} If that is the \emph{signifier}, \click{} what is the \emph{signified}?

	\item The functional choice between them lies not in the content of the quote \emph{per se}, but in the way the narrator chooses to \click{} \emph{stage} the narrative, or in other words, the way she chooses to \emph{package} the quote within the story:

		\begin{itemize}
			\item \click{} The function of the unmarked QI1 is basic signalling of turns in the dialogue, with each instance constituting one turn.
				These roughly imitate in fiction the way dialogues operate in face-to-face conversation.

			\item \click{} QI2 belongs to the dialogue sphere as well, but has an additional marked \emph{narrative} function that relates to the very act or manner of \emph{speaking itself}, which is packaged as pertinent to the story: speech here makes an \emph{event}.

			\item \click{} QI3, unlike the other two, signals that the quote does \emph{not} belong in a dialogue at all.
		\end{itemize}

	\item So \click{} QI1 belongs in dialogue, \click{} QI3 in narrative and \click{} QI2 has one foot in each.

	\item \click{} Given their functions, it is unsurprising that the unmarked QI1 is the most prevalent form, and the other two are much more restricted.

	\item \click{} With this new functional perspective, let’s look at the above examples and others:
\end{itemize}


\partitle{\click{} Functional perspective: QI1}
\begin{itemize}
	\item \click{} Here we can see instances of QI1 marking each one turn in a simple conversation.%, which goes on after this fragment.

	\item \click{} There are some conversations that consist of very long sequences of QI1s.
\end{itemize}


\partitle{Functional perspective: QI2}
\begin{itemize}
	\item \click{} QI2: speech as an event.

	\item \click{} Here in the narrative segment Bilw is laughing and everybody joins him.
		\click{} Their inability to speak because of this is explicitly referred to in the text.
		That is changed when Begw \click{} stops and asks a question.
		QI2 is used here for signalling the narrative pertinence of overcoming an obstacle to speaking.

	\item \click{} Here is another example, when Begw \click{} ventures to speak after she broke into tears.

	\item \click{} Here we can see a case of an explicit reference to the very act of speaking: \click{} \C{‘Fel pe na bai’n dweud dim byd, ac er mwyn dweud rhywbeth’}.

	% \item Additional uses include cases like \click{} this, where the speaker does not plainly mean to speak with \click{} the addressee, but speaks in a sarcastic, passive-aggressive manner that is directed to \click{} Mrs Huws in reality. This is a kind of narrative special effects: think a \emph{wide shot} instead of a \emph{medium shot}.
		% \click{} A hypothetical QI1 pattern would suggest she earnestly speaks to Robin.
\end{itemize}


\partitle{\click{} Functional perspective: QI3}
\begin{itemize}
	\item \click{} Here with QI3 the short imitative quote \C{‘pws pws’} is not a part of any dialogue but a call made by the protagonist in order to find her cat.

	\item \click{} We see QI3 every time there is speech outside of a dialogue:

		\begin{itemize}
			\item \click{} This can be simple events in the plot, such as \click{} saying ‘good night’;

			\item \click{} or something someone \click{} \emph{would have said} hypothetically;

			\item \click{} speech that repeats iteratively within the plot line;

			\item \click{} or outside the plot line: \click{} ‘he would shout ‘again’ all the time’.
		\end{itemize}
		
	\item \click{} The quotes that are introduced with QI3 are usually quite limited in length and complexity, mostly interjections and the like.
	% \item \click{} One exception is the irrealis example \click{} we’ve seen earlier, where a character imagines what her mother would have said.
\end{itemize}




\partitle{Nucleus}
\begin{itemize}
	\item \click{} On to the next section: the nucleus.

	\item \click{} As mentioned before, the different types differ not only in their syntagmatic structure and their function, \click{} but also in the paradigmatic properties of the slots that make the patterns.

	\item Much can be said about the text-linguistic features of all the slots, \click{} but in a 20-minute talk we’ll focus on the nucleus only.
\end{itemize}


\partitle{Verbal}
\begin{itemize}
	\item Güldemann distinguishs between \click{} generic speech verbs like \E{say} in English and \click{} specific ones like \E{answer}, \E{ask} or \E{whisper}, which add specific semantic information.
\end{itemize}


\partitle{Generic speech verbs}
\begin{itemize}
	\item \click{} Three Welsh forms could possibly be described as generic speech verbs in Literary Welsh: \C{ebe}, \C{meddai} and \C{dweud}.

	\item However, while \C{dweud} is a normal verb, \click{} \C{ebe} and \C{meddai} are quite restricted and abnormal, in a way that makes one think whether they should be in fact categorised as ‘verbs’ at all.
		We won’t go into details, \click{} but the main point is that \C{ebe} is the most restricted and conventionalised form while \C{dweud} is a fully-fledged verb.
		\click{} This correlates with the quotative index types:
		\begin{itemize}
			\item \click{} The dialogic QI1 has \C{ebe} and \C{meddai} but not \C{dweud}.

			\item \click{} The narrative QI3 has the prototypically verbal \C{dweud} but not the others.

			\item \click{} and QI2 lies in the middle.
		\end{itemize}

	\item \click{} Within the context of QI1 I couldn’t find any \emph{semantic} difference between \C{ebe} and \C{meddai}.
		If you have any insights about this please talk with me afterwards!
		Being curious about this is what initiated this whole investigation for me…

	\item \click{} Anyway, counting their occurrences reveals an uneven distribution, and in total \C{meddai} is far more common than \C{ebe}.
		In contemporary literature \C{ebe} is nearly non-existent.
		
	\item \click{} An anecdote. In her memoir Roberts mentioned an acquaintance who used to use the related archaic \C{ebra fo} or \C{bro fi} when telling stories, \click{} so maybe she too put some ‘\C{ebe}-currants’ in her stories as well…
		
\end{itemize}


% \partitle{Specific speech verbs}
% \begin{itemize}
% 	\item \click{} On to \emph{specific} speech verbs.
% 
% 	\item \click{} A number of them are attested in the corpus, notably \C{gofyn} ‘to ask’ and \C{gweiddi} ‘to shout’.
% 		They can be categorised into ones that specify the \click{} manner a character speaks (for example, \E{shout}) or the \click{} relation to the plot or discourse (for example, \E{ask} or \E{answer}).
% \end{itemize}


\partitle{\C{oddi wrth}}
\begin{itemize}
	\item \click{} An interesting nucleus that is undoubtedly non-verbal is the composite preposition \C{oddi wrth}: literally \click{} ‘“quote” \click{} from \click{} speaker’.

	\item It is quite rare and is reactive in nature, with short and simple quotes.

	\item \click{} For example, here Begw is crying and her father’s unempathetic reaction is \C{‘Taw â chlegar’}.

	\item And \click{} here everyone’s reaction to Bilw’s shocking story is \C{‘O’}.
\end{itemize}


\partitle{Nominal predication}
\begin{itemize}
	\item \click{} Another rare nucleus is the nominal predication, equating the quote with a noun such as \C{cyfarchiad} ‘greeting’ or \C{geiriau} ‘words’.

	\item \click{} Here for example we have \click{} a quote \click{} that was \click{} Elin Gruffydd’s greeting.

	\item \click{} In all the cases in the corpus the quote that is introduced by this construction is the first words the speaker says in the scene.
		This is strengthen in some cases by the semantics of \click{} \C{cyfarchiad} ‘greeting’ or \click{} the adjective \C{cyntaf} ‘first’.
\end{itemize}


\partitle{\click{} Overt versus zeroed QI1}
\begin{itemize}
	\item \click{} We’ve seen that QI1 can be either \click{} overt (here with nucleus, speaker and modification components) or \click{} zeroed, leaving the typographical markers of quotation marks and paragraph breaks as the only markers.

	\item \click{} The reason to bundle the two together under the same type is that they both signal unmarked, basic turns in the dialogue, and are best described two subtypes (or \emph{allotypes}) of the same fundamental type.

	\item What is the distribution between them then? \click{} The zeroed subtype is the default, unless something triggers the full overt one:

		\begin{itemize}
			\item \click{} When the identification of the speaker is not unambiguous from the previous context.

			\item \click{} Or when the author describes something specific \click{} through modification (for example, \C{meddai yn swil} ‘said shyly’) or \click{} through a specific speech verb (for example, \C{gwaeddodd} ‘shouted’).
		\end{itemize}

	\item \click{} That rule means that when two characters converse \click{} we can have long sequences of zeroed QI1s.

	\item \click{} When more than two characters converse we see many occurrences of overt QI1s, and interestingly if a character becomes less involved in a three-party conversation this pragmatic development is expressed in the fictional dialogue by the transition to zeroed QI1s.
\end{itemize}


\partitle{\click{} Conclusion}
\begin{itemize}
	\item \click{} So to wrap up the \emph{textile} we weaved in this talk: \click{} we examined the linguistic aspects of how two main threads~— or \emph{modes}~— of narrative \emph{texts} are interwoven in a complex and intricate manner by the \C{brenhines} of short stories in Welsh.

	\item \click{} A key observation is the distinction between three types of quotative indexes, each weaving the quote into the fabric of the text differently.
\end{itemize}


\partitle{Further research}
\begin{itemize}
	\item \click{} What’s next?
		I find two question interesting and fruitful.

	\item \click{} One is how much is shared between the system that emerges from the studied corpus and other Literary Welsh works?
		What about narratives told in Colloquial Welsh, which are largely underresearched?

	\item \click{} The other is how does what we see in Welsh stand in comparison to other languages?
		Some phenomena~— such the fossilisation of quotative forms like \C{ebe} and \C{meddai}~— are well documented in other languages.
		% Typological research on reported speech in general has been conducted, but the specifics of reported speech in narrative are not thoroughly explored from a typological perspective.

	\item \click{} With these two open questions I’d like to thank you \C{o galon}, looking forward to \emph{your} questions. \click{}
\end{itemize}

\end{document}
